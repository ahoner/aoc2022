#![feature(drain_filter)]
#![feature(int_abs_diff)]
#![feature(destructuring_assignment)]

mod days;
mod etc;

use days::{
    day01,
    day02,
    day03,
    day04,
    day05,
    day06,
    day07,
    day08,
    day09,
    day10,
    day11,
    day12,
};
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        panic!("You must provide the day to run.");
    }

    let days: Vec<u8> = args.iter()
        .skip(1)
        .map(|d| d.parse().unwrap_or_else(|v| panic!("Not a valid day: {}", v)))
        .collect();

    for day in days {
        let func = match day {
            1 => day01::run,
            2 => day02::run,
            3 => day03::run,
            4 => day04::run,
            5 => day05::run,
            6 => day06::run,
            7 => day07::run,
            8 => day08::run,
            9 => day09::run,
            10 => day10::run,
            11 => day11::run,
            12 => day12::run,
            _=> panic!("Day not implemented."),
        };

        println!("\n--- Day {:02} ---", day);
        func();
    }
}
