use crate::etc::utils::input;

use std::time::{ Instant };

pub fn run() {
    let input: String = input::fetch_input("10".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut sum: isize = 0;
    let mut reg: isize = 1;
    let mut command_index: usize = 0;
    let mut running_add: bool = false;
    let input: Vec<&str> = input.trim()
        .split("\n")
        .collect();
    for cycle in 1..221 {
        if (cycle - 20) % 40 == 0 {
            sum += reg * cycle;
        } 
        let args: Vec<&str> = input[command_index].split(" ").collect();
        let current_command: Option<isize> = if args[0] == "noop" { None } else { Some(args[1].parse().unwrap()) };
        match current_command {
            None => command_index += 1,
            Some(n) => {
                if !running_add {
                    running_add = true;
                } else {
                    running_add = false;
                    reg += n;
                    command_index += 1;
                }
            }
        }
    }
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
    println!("Sum of cycles: {}", sum);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut reg: isize = 1;
    let mut command_index: usize = 0;
    let mut running_add: bool = false;
    let mut current_line: Vec<&str> = vec![];
    let mut current_draw_pos: isize = 0;
    let mut cycle = 1;
    let input: Vec<&str> = input.trim()
        .split("\n")
        .collect();
    while command_index < input.len() {
        let args: Vec<&str> = input[command_index].split(" ").collect();
        let current_command: Option<isize> = if args[0] == "noop" { None } else { Some(args[1].parse().unwrap()) };
        if cycle % 41 == 0 {
            println!("{}", current_line.join(""));
            current_line = vec![];
            current_draw_pos = 0;
            cycle += 1;
            continue;
        }
        if current_draw_pos == reg - 1 || current_draw_pos == reg + 1 || current_draw_pos == reg {
            current_line.push("#");
        } else {
            current_line.push(".");
        }
        current_draw_pos += 1;
        match current_command {
            None => command_index += 1,
            Some(n) => {
                if !running_add {
                    running_add = true;
                } else {
                    running_add = false;
                    reg += n;
                    command_index += 1;
                }
            }
        }
        cycle += 1;
    }
    println!("{}", current_line.join(""));
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
