use crate::etc::utils::input;

use std::time::{ Instant };

pub fn run() {
    let input: String = input::fetch_input("8".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let input: Vec<Vec<usize>> = input
        .trim()
        .split("\n")
        .map(|l| l
            .split("")
            .filter(|c| *c != "")
            .map(|c| c.parse().unwrap())
            .collect())
        .collect();
    let mut sum: usize = 0;
    let width = input[0].len();
    let height = input.len();
    for y in 1..height - 1 {
        for x in 1..width - 1 {
            let result = ray_cast(&input, x, y);
            if result { sum += 1; }
        }
    }
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
    let perimeter = (width - 2) * 2 + (height - 2) * 2 + 4;
    println!("Visible Trees: {}", perimeter + sum);
}

fn ray_cast(input: &Vec<Vec<usize>>, x: usize, y: usize) -> bool {
    let orgin_val = input[y][x];
    for i in (0..y).rev() {
        if i == 0 && input[i][x] < orgin_val { return true; }
        if input[i][x] >= orgin_val { break; }
    }
    for i in (0..x).rev() {
        if i == 0 && input[y][i] < orgin_val { return true; }
        if input[y][i] >= orgin_val { break; }
    }
    for i in y + 1..input.len() {
        if i == input.len() - 1 && input[i][x] < orgin_val { return true; }
        if input[i][x] >= orgin_val { break; }
    }
    for i in x + 1..input[0].len() {
        if i == input[0].len() - 1 && input[y][i] < orgin_val { return true; }
        if input[y][i] >= orgin_val { break; }
    }
    return false;
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let input: Vec<Vec<usize>> = input
        .trim()
        .split("\n")
        .map(|l| l
            .split("")
            .filter(|c| *c != "")
            .map(|c| c.parse().unwrap())
            .collect())
        .collect();
    let width = input[0].len();
    let height = input.len();
    let mut max_score: usize = 0;
    for y in 1..height - 1 {
        for x in 1..width - 1 {
            let current_score = scenic_score(&input, x, y);
            if current_score > max_score { max_score = current_score; }
        }
    }
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
    println!("Highest Scenic Score: {} points", max_score);
}

fn scenic_score(input: &Vec<Vec<usize>>, x: usize, y: usize) -> usize {
    let mut score: usize = 1;
    let orgin_val = input[y][x];
    let mut count = 0;
    for i in (0..y).rev() {
        count += 1;
        if input[i][x] >= orgin_val { break; }
    }
    score *= count;
    count = 0;
    for i in (0..x).rev() {
        count += 1;
        if input[y][i] >= orgin_val { break; }
    }
    score *= count;
    count = 0;
    for i in y + 1..input.len() {
        count += 1;
        if input[i][x] >= orgin_val { break; }
    }
    score *= count;
    count = 0;
    for i in x + 1..input[0].len() {
        count += 1;
        if input[y][i] >= orgin_val { break; }
    }
    score *= count;
    return score;
}
