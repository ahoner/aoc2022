use crate::etc::utils::input;

use std::time::{ Instant };
use regex::Regex;

pub fn run() {
    let input: String = input::fetch_input("5".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let input: Vec<&str> = input.split("\n\n").collect();
    let cargo: Vec<&str> = input[0].split("\n").collect();
    let instructions: Vec<&str> = input[1].trim().split("\n").collect();
    let num_stacks = (cargo[0].len() + 1) / 4;
    let mut stacks: Vec<Vec<char>> = Vec::new();
    for _i in 0..num_stacks {
        stacks.push(Vec::new());
    }
    for (i, line) in cargo.iter().rev().enumerate() {
        if i == 0 { continue; }
        for i in 0..num_stacks {
            let curr_crate = line.as_bytes()[4 * i + 1] as char;
            if curr_crate == ' ' { continue }
            stacks[i].push(curr_crate);
        }
    }

    let re = Regex::new(r"^move (\d+) from (\d+) to (\d+)$").unwrap();
    for line in instructions {
        for cap in re.captures_iter(line) {
            let quantity: usize = cap[1].parse().expect("Not a number");
            let start: usize = cap[2].parse().expect("Not a number");
            let dest: usize = cap[3].parse().expect("Not a number");
            for _i in 0..quantity {
                let c = stacks[start - 1].pop().unwrap();
                stacks[dest - 1].push(c);
            }
        }
    }
    let mut output = String::from("");
    for i in 0..stacks.len() {
        output.push(stacks[i].pop().unwrap());
    }
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
    println!("{}", output);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let input: Vec<&str> = input.split("\n\n").collect();
    let cargo: Vec<&str> = input[0].split("\n").collect();
    let instructions: Vec<&str> = input[1].trim().split("\n").collect();
    let num_stacks = (cargo[0].len() + 1) / 4;
    let mut stacks: Vec<Vec<char>> = Vec::new();
    for _i in 0..num_stacks {
        stacks.push(Vec::new());
    }
    for (i, line) in cargo.iter().rev().enumerate() {
        if i == 0 { continue; }
        for i in 0..num_stacks {
            let curr_crate = line.as_bytes()[4 * i + 1] as char;
            if curr_crate == ' ' { continue }
            stacks[i].push(curr_crate);
        }
    }

    let re = Regex::new(r"^move (\d+) from (\d+) to (\d+)$").unwrap();
    for line in instructions {
        for cap in re.captures_iter(line) {
            let quantity: usize = cap[1].parse().expect("Not a number");
            let start: usize = cap[2].parse().expect("Not a number");
            let dest: usize = cap[3].parse().expect("Not a number");
            let mut tmp: Vec<char> = Vec::new();
            for _i in 0..quantity {
                let c = stacks[start - 1].pop().unwrap();
                tmp.push(c);
            }
            for _i in 0..tmp.len() {
                stacks[dest - 1].push(tmp.pop().unwrap());
            }
        }
    }
    let mut output = String::from("");
    for i in 0..stacks.len() {
        output.push(stacks[i].pop().unwrap());
    }
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
    println!("{}", output);
}
