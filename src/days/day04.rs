use crate::etc::utils::input;

use std::time::{ Instant };

pub fn run() {
    let input: String = input::fetch_input("4".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let input: Vec<&str> = input.trim().split("\n").collect();
    let mut sum = 0;
    for line in input {
        let ranges: Vec<i32> = line.replace(",", "-").split("-").map(|m| m.parse().expect("not a number")).collect();
        if (ranges[0] <= ranges[2] && ranges[1] >= ranges[3]) ||
            (ranges[2] <= ranges[0] && ranges[3] >= ranges[1]) {
            sum += 1;
        }
    }
    println!("Time Elapsed: {}ms\nTotal overlapping: {}", part_1_time.elapsed().as_micros() as f32 / 1000.0, sum);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let input: Vec<&str> = input.trim().split("\n").collect();
    let mut sum = 0;
    for line in input {
        let ranges: Vec<i32> = line.replace(",", "-").split("-").map(|m| m.parse().expect("not a number")).collect();
        if (ranges[1] >= ranges[2]) && (ranges[3] >= ranges[0]) {
            sum += 1;
        }
    }
    println!("Time Elapsed: {}ms\nTotal overlapping: {}", part_2_time.elapsed().as_micros() as f32 / 1000.0, sum);
}
