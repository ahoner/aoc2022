use crate::etc::utils::input;

use std::time::{ Instant };

#[derive(Debug)]
struct FileSystem {
    files: Vec<Node>
}

#[derive(Debug, Clone)]
struct Node {
    val: usize,
    parent: Option<usize>,
    children: Vec<usize>
}

impl FileSystem {
    fn insert(&mut self, val: usize, parent: Option<usize>) -> Option<usize> {
        let i = self.files.len();
        if let Some(parent) = parent {
            self.files[parent].children.push(i);
            self.update_size(Some(parent), val);
        }

        self.files.push(Node {
            val,
            parent,
            children: vec![]
        });
        Some(i)
    }

    fn node (&self, i: Option<usize>) -> &Node {
        match i {
            Some(i) => &self.files[i],
            None => panic!("Node does not exist")
        }
    }

    fn update_size(&mut self, index: Option<usize>, size: usize) {
        let mut current = index;
        while let Some(index) = current {
            let node = &mut self.files[index];
            node.val += size;
            current = node.parent;
        }
    }
}

const FILE_SIZE_LIMIT: usize = 100000;
const DISK_SPACE_AVAILABLE: usize = 70000000;
const UNUSED_DISK_SPACE_LIMIT: usize = 30000000;

pub fn run() {
    let input: String = input::fetch_input("7".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let input: Vec<&str> = input.trim().split("\n").collect();
    let mut file_system = FileSystem { files: vec![]};
    let mut current: Option<usize> = None;
    for line in input {
        let args: Vec<&str> = line.split(" ").collect();
        match args[0] {
            "$" => {
                match args[1] {
                    "cd" => {
                        match args[2] {
                            ".." => {
                                current = file_system.node(current).parent;
                            },
                            _ => {
                                current = file_system.insert(0, current);
                            }
                        }
                    },
                    _ => {}
                }
            },
            "dir" => {}
            _ => {
                file_system.insert(args[0].parse().unwrap(), current);
            }
        }
    }
    let dirs: Vec<&Node> = file_system
        .files
        .iter()
        .filter(|n| n.children.len() > 0)
        .collect();
    let sum: usize = dirs
        .iter()
        .map(|n| n.val)
        .filter(|v| *v <= FILE_SIZE_LIMIT)
        .sum();
    println!("{:?}", sum);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let input: Vec<&str> = input.trim().split("\n").collect();
    let mut file_system = FileSystem { files: vec![]};
    let mut current: Option<usize> = None;
    for line in input {
        let args: Vec<&str> = line.split(" ").collect();
        match args[0] {
            "$" => {
                match args[1] {
                    "cd" => {
                        match args[2] {
                            ".." => {
                                current = file_system.node(current).parent;
                            },
                            _ => {
                                current = file_system.insert(0, current);
                            }
                        }
                    },
                    _ => {}
                }
            },
            "dir" => {}
            _ => {
                file_system.insert(args[0].parse().unwrap(), current);
            }
        }
    }
    let dirs: Vec<&Node> = file_system
        .files
        .iter()
        .filter(|n| n.children.len() > 0)
        .collect();
    let delete_threshold = UNUSED_DISK_SPACE_LIMIT - (DISK_SPACE_AVAILABLE - dirs[0].val);
    let sum = dirs
        .iter()
        .map(|n| n.val)
        .filter(|v| *v >= delete_threshold)
        .min()
        .unwrap();
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
    println!("{}", sum);
}
