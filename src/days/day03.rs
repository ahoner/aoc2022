use crate::etc::utils::input;

use std::time::{ Instant };
use std::collections::HashMap;

pub fn run() {
    let input: String = input::fetch_input("3".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let input: Vec<&str> = input.trim().split("\n").collect();
    // I JUST USED A VIM MACRO TO MAKE THIS, DON'T MAKE FUN OF ME
    let value_dict: HashMap<char, i32> = HashMap::from([
        ('a', 1),
        ('b', 2),
        ('c', 3),
        ('d', 4),
        ('e', 5),
        ('f', 6),
        ('g', 7),
        ('h', 8),
        ('i', 9),
        ('j', 10),
        ('k', 11),
        ('l', 12),
        ('m', 13),
        ('n', 14),
        ('o', 15),
        ('p', 16),
        ('q', 17),
        ('r', 18),
        ('s', 19),
        ('t', 20),
        ('u', 21),
        ('v', 22),
        ('w', 23),
        ('x', 24),
        ('y', 25),
        ('z', 26),
        ('A', 26 + 1),
        ('B', 26 + 2),
        ('C', 26 + 3),
        ('D', 26 + 4),
        ('E', 26 + 5),
        ('F', 26 + 6),
        ('G', 26 + 7),
        ('H', 26 + 8),
        ('I', 26 + 9),
        ('J', 26 + 10),
        ('K', 26 + 11),
        ('L', 26 + 12),
        ('M', 26 + 13),
        ('N', 26 + 14),
        ('O', 26 + 15),
        ('P', 26 + 16),
        ('Q', 26 + 17),
        ('R', 26 + 18),
        ('S', 26 + 19),
        ('T', 26 + 20),
        ('U', 26 + 21),
        ('V', 26 + 22),
        ('W', 26 + 23),
        ('X', 26 + 24),
        ('Y', 26 + 25),
        ('Z', 26 + 26),
    ]);
    let sum_list: Vec<i32> = input.iter().map(|m| {
        let (first, second) = m.split_at(m.len() / 2);
        for c in second.chars() {
            if first.contains(c) {
                return *value_dict.get(&c).unwrap();
            }
        }
        return 0;
    }).collect();
    let sum: i32 = sum_list.iter().sum();
    println!("Time Elapsed: {}ms.\nTotal: {}", part_1_time.elapsed().as_micros() as f32 / 1000.0, sum);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    // I JUST USED A VIM MACRO TO MAKE THIS, DON'T MAKE FUN OF ME
    let value_dict: HashMap<char, i32> = HashMap::from([
        ('a', 1),
        ('b', 2),
        ('c', 3),
        ('d', 4),
        ('e', 5),
        ('f', 6),
        ('g', 7),
        ('h', 8),
        ('i', 9),
        ('j', 10),
        ('k', 11),
        ('l', 12),
        ('m', 13),
        ('n', 14),
        ('o', 15),
        ('p', 16),
        ('q', 17),
        ('r', 18),
        ('s', 19),
        ('t', 20),
        ('u', 21),
        ('v', 22),
        ('w', 23),
        ('x', 24),
        ('y', 25),
        ('z', 26),
        ('A', 26 + 1),
        ('B', 26 + 2),
        ('C', 26 + 3),
        ('D', 26 + 4),
        ('E', 26 + 5),
        ('F', 26 + 6),
        ('G', 26 + 7),
        ('H', 26 + 8),
        ('I', 26 + 9),
        ('J', 26 + 10),
        ('K', 26 + 11),
        ('L', 26 + 12),
        ('M', 26 + 13),
        ('N', 26 + 14),
        ('O', 26 + 15),
        ('P', 26 + 16),
        ('Q', 26 + 17),
        ('R', 26 + 18),
        ('S', 26 + 19),
        ('T', 26 + 20),
        ('U', 26 + 21),
        ('V', 26 + 22),
        ('W', 26 + 23),
        ('X', 26 + 24),
        ('Y', 26 + 25),
        ('Z', 26 + 26),
    ]);
    let input: Vec<&str> = input.trim().split("\n").collect();
    let mut sum = 0;
    for i in (0..input.len()).step_by(3) {
        for c in input[i].chars() {
            if input[i + 1].contains(c) && input[i + 2].contains(c) {
                sum += value_dict.get(&c).unwrap();
                break;
            }
        }
    } 
    println!("Time Elapsed: {}ms.\nTotal: {}", part_2_time.elapsed().as_micros() as f32 / 1000.0, sum);
}
