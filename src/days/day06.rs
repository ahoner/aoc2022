use crate::etc::utils::input;

use std::time::{ Instant };
use fancy_regex::Regex;
use substring::Substring;

pub fn run() {
    let input: String = input::fetch_input("6".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let input: &str = input.trim();
    let re = Regex::new(r"^\w*(\w)\w*\1\w*$").unwrap();
    let mut chars = 4;
    for i in 4..input.len() {
        let substr = (*input).substring(i - 4, i);
        let result = re.is_match(substr).unwrap();
        if !result {
            break;
        }
        chars += 1;
    }
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
    println!("Result: {}", chars);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let input: &str = input.trim();
    let re = Regex::new(r"^\w*(\w)\w*\1\w*$").unwrap();
    let mut chars = 14;
    for i in 14..input.len() {
        let substr = (*input).substring(i - 14, i);
        let result = re.is_match(substr).unwrap();
        if !result {
            break;
        }
        chars += 1;
    }
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
    println!("Result: {}", chars);
}
