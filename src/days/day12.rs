use crate::etc::utils::input;

use std::time::{ Instant };
use std::collections::{ HashSet, HashMap };

const START_CODE: u8 = 83;
const END_CODE: u8 = 69;
const A_CODE: u8 = 97;
const B_CODE: u8 = 98;
const Y_CODE: u8 = 121;
const Z_CODE: u8 = 122;

pub fn run() {
    let input: String = input::fetch_input("12".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut start = (0, 0);
    let mut end = (0, 0);
    let input: Vec<Vec<u8>> = input
        .trim()
        .split("\n")
        .map(|l| Vec::from(l.to_string().as_bytes()))
        .collect();
    for (y, line) in input.iter().enumerate() {
        for (x, c) in line.iter().enumerate() {
            if *c == START_CODE {
                start = (x as isize, y as isize);
            } else if *c == END_CODE {
                end = (x as isize, y as isize);
            }
        }
    }

    let came_from = a_star(&input, start, end);

    let mut current = end;
    let mut path = vec![];
    while came_from.contains_key(&current) {
        path.push(current);
        current = *came_from.get(&current).unwrap();
    }
    println!("{:?}", path.len());
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn a_star(map: &Vec<Vec<u8>>, start: (isize, isize), end: (isize, isize)) -> HashMap<(isize, isize), (isize, isize)> {
    let mut open_set: HashSet<(isize, isize)> = HashSet::new();
    open_set.insert(start);
    let mut came_from: HashMap<(isize, isize), (isize, isize)> = HashMap::new();
    let mut g_score: HashMap<(isize, isize), usize> = HashMap::new();
    g_score.insert(start, 0); 
    let mut f_score: HashMap<(isize, isize), usize> = HashMap::new();
    f_score.insert(start, dist(start, end)); 

    while open_set.len() > 0 {
        let mut current: Option<(isize, isize)> = None;
        let mut min_score = usize::MAX;
        for point in open_set.iter() {
            let current_score = f_score.get(point);
            if current_score == None { continue; }
            if min_score > *current_score.unwrap() {
                current = Some(*point);
                min_score = *current_score.unwrap();
            }
        }
        if current.unwrap() == end {
            return came_from;
        }
        let current = current.unwrap();
        open_set.remove(&current);
        let neighbors_list = neighbors(&map, current);
        for n in neighbors_list {
            let tentative_score = if g_score.contains_key(&current) { g_score.get(&current).unwrap() + 1 } else { usize::MAX };
            let neighbor_score = if g_score.contains_key(&n) { *g_score.get(&n).unwrap() } else { usize::MAX };
            if neighbor_score > tentative_score {
                came_from.insert(n, current);
                g_score.insert(n, tentative_score);
                f_score.insert(n, tentative_score + dist(n, end));
                if !open_set.contains(&n) {
                    open_set.insert(n);
                }
            }
        }
    }
    panic!("No path");
}

fn neighbors(map: &Vec<Vec<u8>>, (x, y): (isize, isize)) -> Vec<(isize, isize)> {
    vec![(x, y + 1), (x + 1, y), (x, y - 1), (x - 1, y)]
        .into_iter()
        .filter(|(current_x, current_y)| {
            if 0 > *current_x || 0 > *current_y || *current_y >= map.len() as isize || *current_x >= map[0].len() as isize {
                return false;
            }
            let current_val = map[*current_y as usize][*current_x as usize] as isize;
            let orgin_val = map[y as usize][x as usize] as isize;
            if map[*current_y as usize][*current_x as usize] == A_CODE &&
                map[y as usize][x as usize] == START_CODE {
                return true;
            }
            if map[*current_y as usize][*current_x as usize] == B_CODE &&
                map[y as usize][x as usize] == START_CODE {
                return true;
            }
            if map[*current_y as usize][*current_x as usize] == START_CODE &&
                map[y as usize][x as usize] == A_CODE {
                return true;
            }
            if map[*current_y as usize][*current_x as usize] == END_CODE &&
                map[y as usize][x as usize] == Z_CODE {
                return true;
            }
            if map[*current_y as usize][*current_x as usize] == END_CODE &&
                map[y as usize][x as usize] == Y_CODE {
                return true;
            }
            if current_val - orgin_val == 1 {
                return true;
            } else if 0 >= current_val - orgin_val {
                return true;
            }
            return false;
        })
        .collect()
}

fn dist((x1, y1): (isize, isize), (x2, y2): (isize, isize)) -> usize {
    ((x2 - x1).abs() + (y2 - y1).abs()) as usize
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut starting_points: Vec<(isize, isize)> = vec![];
    let mut end = (0, 0);
    let input: Vec<Vec<u8>> = input
        .trim()
        .split("\n")
        .map(|l| Vec::from(l.to_string().as_bytes()))
        .collect();
    for (y, line) in input.iter().enumerate() {
        for (x, c) in line.iter().enumerate() {
            if *c == END_CODE {
                end = (x as isize, y as isize);
            }
        }
    }
    for i in 0..input.len() {
        starting_points.push((0, i as isize));
    }
    let mut starting_dists: Vec<usize> = starting_points
        .iter()
        .map(|start| {
            let came_from = a_star(&input, *start, end);
            let mut current = end;
            let mut path = vec![];
            while came_from.contains_key(&current) {
                path.push(current);
                current = *came_from.get(&current).unwrap();
            }
            path.len()
        })
        .collect();
    starting_dists.sort();
    println!("{:?}", starting_dists[0]);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
