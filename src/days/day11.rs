use crate::etc::utils::input;

use std::time::{ Instant };

#[derive(Debug)]
enum Operation {
    Plus(u128),
    Mult(u128),
    Exp
}

#[derive(Debug)]
struct Monke {
    operation: Operation,
    test: u128,
    true_monke: usize,
    false_monke: usize
}

pub fn run() {
    let input: String = input::fetch_input("11".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut items: Vec<Vec<u128>> = vec![];
    let mut inspect_count: Vec<usize> = vec![];
    let input: Vec<Monke> = input
        .trim()
        .split("\n\n")
        .map(|m| {
            let monke: Vec<&str> = m.split("\n").collect();
            let current_items: Vec<u128> = monke[1]
                .split(":")
                .collect::<Vec<&str>>()[1]
                .split(",")
                .collect::<Vec<&str>>()
                .iter()
                .map(|i| i.trim().parse().unwrap())
                .collect();
            items.push(current_items);
            let operation: Vec<&str> = monke[2]
                .split("=")
                .collect::<Vec<&str>>()[1]
                .trim()
                .split(" ")
                .collect();
            let operation: Operation = match operation[1] {
                "+" => Operation::Plus(operation[2].parse().unwrap()),
                "*" => {
                    let result = operation[2].parse();
                    match result {
                        Ok(n) => Operation::Mult(n),
                        Err(_) => Operation::Exp
                    }
                },
                _ => panic!("unabled to parse")
            };
            let test: u128 = monke[3]
                .split("by")
                .collect::<Vec<&str>>()[1]
                .trim()
                .parse()
                .unwrap();
            let true_monke = monke[4]
                .split("monkey")
                .collect::<Vec<&str>>()[1]
                .trim()
                .parse()
                .unwrap();
            let false_monke = monke[5]
                .split("monkey")
                .collect::<Vec<&str>>()[1]
                .trim()
                .parse()
                .unwrap();
            Monke {
                operation: operation,
                test: test,
                true_monke: true_monke,
                false_monke: false_monke
            }
        })
        .collect();
    for _ in 0..input.len() {
        inspect_count.push(0);
    }
    for _ in 0..20 {
        for (i, monke) in input.iter().enumerate() {
            for j in 0..(items[i].len()) {
                let mut current_worry: u128 = items[i][j];
                match input[i].operation {
                    Operation::Plus(n) => current_worry += n,
                    Operation::Mult(n) => current_worry *= n,
                    Operation::Exp => current_worry *= current_worry
                }
                inspect_count[i] += 1;
                current_worry /= 3;
                if current_worry % monke.test == 0 {
                    items[monke.true_monke].push(current_worry);
                } else {
                    items[monke.false_monke].push(current_worry);
                }
            }
            items[i] = vec![];
        }
    }
    inspect_count.sort_by(|a, b| b.cmp(a));
    println!("The two most active monkeys inspected: {:?} and {:?} items", inspect_count[0], inspect_count[1]);
    println!("Answer: {:?}", inspect_count[0] * inspect_count[1]);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut items: Vec<Vec<u128>> = vec![];
    let mut inspect_count: Vec<usize> = vec![];
    let input: Vec<Monke> = input
        .trim()
        .split("\n\n")
        .map(|m| {
            let monke: Vec<&str> = m.split("\n").collect();
            let current_items: Vec<u128> = monke[1]
                .split(":")
                .collect::<Vec<&str>>()[1]
                .split(",")
                .collect::<Vec<&str>>()
                .iter()
                .map(|i| i.trim().parse().unwrap())
                .collect();
            items.push(current_items);
            let operation: Vec<&str> = monke[2]
                .split("=")
                .collect::<Vec<&str>>()[1]
                .trim()
                .split(" ")
                .collect();
            let operation: Operation = match operation[1] {
                "+" => Operation::Plus(operation[2].parse().unwrap()),
                "*" => {
                    let result = operation[2].parse();
                    match result {
                        Ok(n) => Operation::Mult(n),
                        Err(_) => Operation::Exp
                    }
                },
                _ => panic!("unabled to parse")
            };
            let test: u128 = monke[3]
                .split("by")
                .collect::<Vec<&str>>()[1]
                .trim()
                .parse()
                .unwrap();
            let true_monke = monke[4]
                .split("monkey")
                .collect::<Vec<&str>>()[1]
                .trim()
                .parse()
                .unwrap();
            let false_monke = monke[5]
                .split("monkey")
                .collect::<Vec<&str>>()[1]
                .trim()
                .parse()
                .unwrap();
            Monke {
                operation: operation,
                test: test,
                true_monke: true_monke,
                false_monke: false_monke
            }
        })
        .collect();
    for _ in 0..input.len() {
        inspect_count.push(0);
    }
    let common_denominator = input.iter().fold(1, |acc, curr| acc * curr.test);
    for _ in 0..10000 {
        for (i, monke) in input.iter().enumerate() {
            for j in 0..(items[i].len()) {
                let mut current_worry: u128 = items[i][j];
                match input[i].operation {
                    Operation::Plus(n) => current_worry += n,
                    Operation::Mult(n) => current_worry *= n,
                    Operation::Exp => current_worry *= current_worry
                }
                inspect_count[i] += 1;
                if current_worry % monke.test == 0 {
                    items[monke.true_monke].push(current_worry % common_denominator);
                } else {
                    items[monke.false_monke].push(current_worry % common_denominator);
                }
            }
            items[i] = vec![];
        }
    }
    inspect_count.sort_by(|a, b| b.cmp(a));
    println!("The two most active monkeys inspected: {:?} and {:?} items", inspect_count[0], inspect_count[1]);
    println!("Answer: {:?}", inspect_count[0] * inspect_count[1]);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
