use crate::etc::utils::input;

use std::time::{ Instant };

pub fn run() {
    let input: String = input::fetch_input("1".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let input: Vec<&str> = input.trim().split("\n\n").collect();
    let largest_sum = input.iter().fold(0, |acc, curr| {
        let elf_stash: Vec<i32> = curr.split("\n").map(|m| m.parse().expect("Not a number")).collect();
        let curr_sum = elf_stash.iter().sum();
        if curr_sum > acc { curr_sum } else { acc }
    });
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
    println!("Largest snack stash: {} calories", largest_sum);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let input: Vec<&str> = input.trim().split("\n\n").collect();
    let mut sums: Vec<i32> = input.iter().map(|m| {
        let elf_stash: Vec<i32> = m.split("\n").map(|m| m.parse().expect("Not a number")).collect();
        elf_stash.iter().sum()
    }).collect();
    sums.sort_by(|a, b| b.cmp(a));
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
    println!("Sum of top three: {} calories", sums[0] + sums[1] + sums[2]);
}
