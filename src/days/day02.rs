use crate::etc::utils::input;

use std::time::{ Instant };

pub fn run() {
    let input: String = input::fetch_input("2".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let sum: Vec<i32> = input.trim().split("\n")
        .map(|l| {
            let line: Vec<&str> = l.split(" ").collect();
            let line = (line[0], line[1]);
            match line {
                ("A", "X") => 4,
                ("A", "Y") => 8,
                ("A", "Z") => 3,
                ("B", "X") => 1,
                ("B", "Y") => 5,
                ("B", "Z") => 9,
                ("C", "X") => 7,
                ("C", "Y") => 2,
                ("C", "Z") => 6,
                _ => 0
            }
        })
        .collect();
    let sum: i32 = sum.iter().sum();
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
    println!("Sum of all games: {} points!", sum);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let sum: Vec<i32> = input.trim().split("\n")
        .map(|l| {
            let line: Vec<&str> = l.split(" ").collect();
            let line = (line[0], line[1]);
            match line {
                ("A", "X") => 3,
                ("A", "Y") => 4,
                ("A", "Z") => 8,
                ("B", "X") => 1,
                ("B", "Y") => 5,
                ("B", "Z") => 9,
                ("C", "X") => 2,
                ("C", "Y") => 6,
                ("C", "Z") => 7,
                _ => 0
            }
        })
        .collect();
    let sum: i32 = sum.iter().sum();
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
    println!("Sum of all games: {} points!", sum);
}
