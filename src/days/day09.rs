use crate::etc::utils::input;

use std::time::{ Instant };
use std::collections::{ HashSet };

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right
}

#[derive(Debug)]
struct Command {
    direction: Direction,
    distance: isize
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq)]
struct Point {
    x: isize,
    y: isize
}

impl Point {
    pub fn distance(&self, other: &Self) -> isize {
        return (other.x - self.x).abs() + (other.y - self.y).abs()
    }
}

pub fn run() {
    let input: String = input::fetch_input("9".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let commands: Vec<Command> = input
        .trim()
        .split("\n")
        .map(|l| {
            let args: Vec<&str> = l.split(" ").collect();
            match args[0] {
                "U" => Command { direction: Direction::Up,  distance: args[1].parse().unwrap() },
                "D" => Command { direction: Direction::Down,  distance: args[1].parse().unwrap() },
                "L" => Command { direction: Direction::Left,  distance: args[1].parse().unwrap() },
                "R" => Command { direction: Direction::Right,  distance: args[1].parse().unwrap() },
                _ => panic!("Cannot parse")
            }
        })
        .collect();
    let mut head = Point { x: 0, y: 0 };
    let mut tail = Point { x: 0, y: 0 };
    let mut visited: HashSet<Point> = HashSet::new();
    for command in commands {
        match command.direction {
            Direction::Up => {
                for _i in 0..command.distance { head.y += 1; update(head, &mut tail); visited.insert(tail); }
            },
            Direction::Down => {
                for _i in 0..command.distance { head.y -= 1; update(head, &mut tail); visited.insert(tail); }
            },
            Direction::Left => {
                for _i in 0..command.distance { head.x -= 1; update(head, &mut tail); visited.insert(tail); }
            },
            Direction::Right => {
                for _i in 0..command.distance { head.x += 1; update(head, &mut tail); visited.insert(tail); }
            }
        }
    }
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
    println!("The tail visited: {:?} unique spaces", visited.len());
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let commands: Vec<Command> = input
        .trim()
        .split("\n")
        .map(|l| {
            let args: Vec<&str> = l.split(" ").collect();
            match args[0] {
                "U" => Command { direction: Direction::Up,  distance: args[1].parse().unwrap() },
                "D" => Command { direction: Direction::Down,  distance: args[1].parse().unwrap() },
                "L" => Command { direction: Direction::Left,  distance: args[1].parse().unwrap() },
                "R" => Command { direction: Direction::Right,  distance: args[1].parse().unwrap() },
                _ => panic!("Cannot parse")
            }
        })
        .collect();
    let mut rope: Vec<Point> = vec![];
    let mut visited: HashSet<Point> = HashSet::new();
    for _i in 0..10 {
        rope.push(Point { x: 0, y: 0 });
    }
    for command in commands {
        match command.direction {
            Direction::Up => {
                for _i in 0..command.distance {
                    rope.get_mut(0).unwrap().y += 1;
                    for i in 0..rope.len() - 1 {
                        update(rope[i].clone(), rope.get_mut(i + 1).unwrap());
                    }
                    visited.insert(rope[9]);
                }
            },
            Direction::Down => {
                for _i in 0..command.distance {
                    rope.get_mut(0).unwrap().y -= 1;
                    for i in 0..rope.len() - 1 {
                        update(rope[i].clone(), rope.get_mut(i + 1).unwrap());
                    }
                    visited.insert(rope[9]);
                }
            },
            Direction::Left => {
                for _i in 0..command.distance {
                    rope.get_mut(0).unwrap().x -= 1;
                    for i in 0..rope.len() - 1 {
                        update(rope[i].clone(), rope.get_mut(i + 1).unwrap());
                    }
                    visited.insert(rope[9]);
                }
            },
            Direction::Right => {
                for _i in 0..command.distance {
                    rope.get_mut(0).unwrap().x += 1;
                    for i in 0..rope.len() - 1 {
                        update(rope[i].clone(), rope.get_mut(i + 1).unwrap());
                    }
                    visited.insert(rope[9]);
                }
            }
        }
    }
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
    println!("The tail visited: {:?} unique spaces", visited.len());
}

fn update(current: Point, next: &mut Point) -> Point {
    let dist = current.distance(next);
    if dist == 4 {
        if current.x > next.x && current.y > next.y {
            next.x += 1;
            next.y += 1;
        } else if current.x > next.x && next.y > current.y {
            next.x += 1;
            next.y -= 1;
        } else if next.x > current.x && next.y > current.y {
            next.x -= 1;
            next.y -= 1;
        } else {
            next.x -= 1;
            next.y += 1;
        }
    } else if current.y > next.y {
        if dist == 2 && (current.x == next.x) {
            next.y += 1;
        } else if dist == 3 && current.x > next.x {
            next.x += 1;
            next.y += 1;
        } else if dist == 3 && next.x > current.x {
            next.x -= 1;
            next.y += 1;
        }
    } else if next.y > current.y {
        if dist == 2 && (current.x == next.x) {
            next.y -= 1;
        } else if dist == 3 && current.x > next.x {
            next.x += 1;
            next.y -= 1;
        } else if dist == 3 && next.x > current.x {
            next.x -= 1;
            next.y -= 1;
        }
    } else if next.x > current.x {
        if dist == 2 && (current.y == next.y) {
            next.x -= 1;
        } else if dist == 3 && current.y > next.y {
            next.x -= 1;
            next.y += 1;
        } else if dist == 3 && next.y > current.y {
            next.x -= 1;
            next.y -= 1;
        }
    } else if current.x > next.x {
        if dist == 2 && (current.y == next.y) {
            next.x += 1;
        } else if dist == 3 && current.y > next.y {
            next.x += 1;
            next.y += 1;
        } else if dist == 3 && next.y > current.y {
            next.x += 1;
            next.y -= 1;
        }
    }
    return Point { x: next.x, y: next.y };
}
